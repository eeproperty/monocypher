{
  "targets": [{
    "target_name": "monocypher",
    "include_dirs": [
      "src"
    ],
    "sources": [
      "src/binding.c",
      "src/monocypher.c"
    ]
  }]
}
