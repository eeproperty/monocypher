#include <node_api.h>
#include "monocypher.h"
#include "macros.h"

#define NUM_ARGS_LOCK (4)
#define NUM_ARGS_UNLOCK (5)
#define NUM_ARGS_KEY_EXCHANGE (3)
#define NUM_ARGS_KEY_EXCHANGE_PUBLIC_KEY (2)

#define MAC_SIZE (16)
#define KEY_SIZE (32)

napi_value lock(napi_env env, napi_callback_info info) {
    size_t argc = NUM_ARGS_LOCK;
    napi_value args[NUM_ARGS_LOCK];
    NAPI_CALL(env, napi_get_cb_info(env, info, &argc, args, NULL, NULL));
    NAPI_ASSERT(env, argc >= NUM_ARGS_LOCK, "Wrong number of arguments.");

    bool is_buffer = false;
    NAPI_ASSERT(env, napi_is_buffer(env, args[0], &is_buffer) == napi_ok, "Wrong type of argument 'key'. Expects a Buffer.");
    NAPI_ASSERT(env, napi_is_buffer(env, args[1], &is_buffer) == napi_ok, "Wrong type of argument 'nonce'. Expects a Buffer.");
    NAPI_ASSERT(env, napi_is_buffer(env, args[2], &is_buffer) == napi_ok, "Wrong type of argument 'message'. Expects a Buffer.");

    napi_valuetype type;
    NAPI_CALL(env, napi_typeof(env, args[3], &type));
    NAPI_ASSERT(env, type == napi_function, "Wrong type of argument 'callback'. Expects a function.");

    uint8_t *key;
    NAPI_CALL(env, napi_get_buffer_info(env, args[0], (void **)&key, NULL));

    uint8_t *nonce;
    NAPI_CALL(env, napi_get_buffer_info(env, args[1], (void **)&nonce, NULL));

    uint8_t *message;
    size_t message_size;
    NAPI_CALL(env, napi_get_buffer_info(env, args[2], (void **)&message, &message_size));

    uint8_t cypher[message_size];
    uint8_t mac[MAC_SIZE];
    crypto_lock(mac, cypher, key, nonce, message, message_size);

    napi_value argv[2];
    NAPI_CALL(env, napi_create_buffer_copy(env, MAC_SIZE, mac, NULL, &argv[0]));
    NAPI_CALL(env, napi_create_buffer_copy(env, message_size, cypher, NULL, &argv[1]));

    napi_value global;
    NAPI_CALL(env, napi_get_global(env, &global));
    NAPI_CALL(env, napi_call_function(env, global, args[3], 2, argv, NULL));

    return NULL;
}

napi_value unlock(napi_env env, napi_callback_info info) {
    size_t argc = NUM_ARGS_UNLOCK;
    napi_value args[NUM_ARGS_UNLOCK];
    NAPI_CALL(env, napi_get_cb_info(env, info, &argc, args, NULL, NULL));
    NAPI_ASSERT(env, argc >= NUM_ARGS_UNLOCK, "Wrong number of arguments.");

    bool is_buffer = false;
    NAPI_ASSERT(env, napi_is_buffer(env, args[0], &is_buffer) == napi_ok, "Wrong type of argument 'key'. Expects a Buffer.");
    NAPI_ASSERT(env, napi_is_buffer(env, args[1], &is_buffer) == napi_ok, "Wrong type of argument 'nonce'. Expects a Buffer.");
    NAPI_ASSERT(env, napi_is_buffer(env, args[2], &is_buffer) == napi_ok, "Wrong type of argument 'mac'. Expects a Buffer.");
    NAPI_ASSERT(env, napi_is_buffer(env, args[3], &is_buffer) == napi_ok, "Wrong type of argument 'cypher'. Expects a Buffer.");
    
    napi_valuetype type;
    NAPI_CALL(env, napi_typeof(env, args[4], &type));
    NAPI_ASSERT(env, type == napi_function, "Wrong type of argument 'callback'. Expects a function.");

    uint8_t *key;
    NAPI_CALL(env, napi_get_buffer_info(env, args[0], (void **)&key, NULL));

    uint8_t *nonce;
    NAPI_CALL(env, napi_get_buffer_info(env, args[1], (void **)&nonce, NULL));

    uint8_t *mac;
    NAPI_CALL(env, napi_get_buffer_info(env, args[2], (void **)&mac, NULL));

    uint8_t *cypher;
    size_t message_size;
    NAPI_CALL(env, napi_get_buffer_info(env, args[3], (void **)&cypher, &message_size));

    uint8_t message[message_size];
    napi_value argv[2];
    if (crypto_unlock(message, key, nonce, mac, cypher, message_size) < 0) {
        NAPI_CALL(env, napi_create_string_utf8(env, "Corrupted message.", NAPI_AUTO_LENGTH, &argv[0]));
    } else {
        NAPI_CALL(env, napi_get_undefined(env, &argv[0]));
    }
    NAPI_CALL(env, napi_create_buffer_copy(env, message_size, message, NULL, &argv[1]));

    napi_value global;
    NAPI_CALL(env, napi_get_global(env, &global));
    NAPI_CALL(env, napi_call_function(env, global, args[4], 2, argv, NULL));

    return NULL;
}

napi_value key_exchange(napi_env env, napi_callback_info info) {
    size_t argc = NUM_ARGS_KEY_EXCHANGE;
    napi_value args[NUM_ARGS_KEY_EXCHANGE];
    NAPI_CALL(env, napi_get_cb_info(env, info, &argc, args, NULL, NULL));
    NAPI_ASSERT(env, argc >= NUM_ARGS_KEY_EXCHANGE, "Wrong number of arguments.");

    bool is_buffer = false;
    NAPI_ASSERT(env, napi_is_buffer(env, args[0], &is_buffer) == napi_ok, "Wrong type of argument 'secret_key'. Expects a Buffer.");
    NAPI_ASSERT(env, napi_is_buffer(env, args[1], &is_buffer) == napi_ok, "Wrong type of argument 'public_key'. Expects a Buffer.");

    napi_valuetype type;
    NAPI_CALL(env, napi_typeof(env, args[2], &type));
    NAPI_ASSERT(env, type == napi_function, "Wrong type of argument 'callback'. Expects a function.");

    uint8_t *secret_key;
    NAPI_CALL(env, napi_get_buffer_info(env, args[0], (void **)&secret_key, NULL));

    uint8_t *public_key;
    NAPI_CALL(env, napi_get_buffer_info(env, args[1], (void **)&public_key, NULL));

    uint8_t shared_key[KEY_SIZE];
    crypto_key_exchange(shared_key, secret_key, public_key);

    napi_value argv[1];
    NAPI_CALL(env, napi_create_buffer_copy(env, KEY_SIZE, shared_key, NULL, argv));

    napi_value global;
    NAPI_CALL(env, napi_get_global(env, &global));
    NAPI_CALL(env, napi_call_function(env, global, args[2], 1, argv, NULL));

    return NULL;
}

napi_value key_exchange_public_key(napi_env env, napi_callback_info info) {
    size_t argc = NUM_ARGS_KEY_EXCHANGE_PUBLIC_KEY;
    napi_value args[NUM_ARGS_KEY_EXCHANGE_PUBLIC_KEY];
    NAPI_CALL(env, napi_get_cb_info(env, info, &argc, args, NULL, NULL));
    NAPI_ASSERT(env, argc >= NUM_ARGS_KEY_EXCHANGE_PUBLIC_KEY, "Wrong number of arguments.");

    bool is_buffer = false;
    NAPI_ASSERT(env, napi_is_buffer(env, args[0], &is_buffer) == napi_ok, "Wrong type of argument 'secret_key'. Expects a Buffer.");

    napi_valuetype type;
    NAPI_CALL(env, napi_typeof(env, args[1], &type));
    NAPI_ASSERT(env, type == napi_function, "Wrong type of argument 'callback'. Expects a function.");

    uint8_t *secret_key;
    NAPI_CALL(env, napi_get_buffer_info(env, args[0], (void **)&secret_key, NULL));

    uint8_t public_key[KEY_SIZE];
    crypto_key_exchange_public_key(public_key, secret_key);

    napi_value argv[1];
    NAPI_CALL(env, napi_create_buffer_copy(env, KEY_SIZE, public_key, NULL, argv));

    napi_value global;
    NAPI_CALL(env, napi_get_global(env, &global));
    NAPI_CALL(env, napi_call_function(env, global, args[1], 1, argv, NULL));

    return NULL;
}

napi_value Init(napi_env env, napi_value exports) {
    napi_value fn;

    NAPI_CALL(env, napi_create_function(env, NULL, 0, lock, NULL, &fn));
    NAPI_CALL(env, napi_set_named_property(env, exports, "lock", fn));

    NAPI_CALL(env, napi_create_function(env, NULL, 0, unlock, NULL, &fn));
    NAPI_CALL(env, napi_set_named_property(env, exports, "unlock", fn));

    NAPI_CALL(env, napi_create_function(env, NULL, 0, key_exchange, NULL, &fn));
    NAPI_CALL(env, napi_set_named_property(env, exports, "keyExchange", fn));

    NAPI_CALL(env, napi_create_function(env, NULL, 0, key_exchange_public_key, NULL, &fn));
    NAPI_CALL(env, napi_set_named_property(env, exports, "keyExchangePublicKey", fn));

    return exports;
}

NAPI_MODULE(NODE_GYP_MODULE_NAME, Init)
