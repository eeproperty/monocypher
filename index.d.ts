declare module 'monocypher' {
  function lock(sharedKey: Buffer, nonce: Buffer, message: Buffer, callback: (mac: Buffer, cypher: Buffer) => void): void;
  function unlock(sharedKey: Buffer, nonce: Buffer, mac: Buffer, cypher: Buffer, callback: (err: string, message: Buffer) => void): void;
  function keyExchange(secretKey: Buffer, publicKey: Buffer, callback: (sharedKey: Buffer) => void): void;
  function keyExchangePublicKey(secretKey: Buffer, callback: (publicKey: Buffer) => void): void;
}